//дожидаемся полной загрузки страницы
window.onload = function () {

        
    
    //получаем идентификатор элемента
    var account = document.getElementById('my-account'),
        authorization = document.getElementById('authorization'),
        registration = document.getElementById('registration'),
        close_authorization = document.getElementsByClassName('close')[0],
        close_registration = document.getElementsByClassName('close')[1],
        register_link = document.getElementById('register-link'),
        user_account = document.getElementById('user-account'),
        user_menu = document.getElementById('user-menu'),
        error = document.getElementById('error');
    var temp = 1;
    //нажатие кнопки my-account
    if(account) {
        account.onclick = function() {
           authorization.style.display = "block";
        }
    }
    //нажатие кнопки user_account
    if(user_account) {
        user_account.onclick = function() {
            if (temp > 0 ) {
               user_menu.style.display = "block"; 
               temp = 0;
            } else {
               user_menu.style.display = "none";  
               temp = 1;
            }
            
        }
    }

    //нажатие крестика
    close_authorization.onclick = function() {
        authorization.style.display = "none";
    }
     //нажатие крестика
    close_registration.onclick = function() {
        registration.style.display = "none";
    }
    //переход для регистрации
    register_link.onclick = function() {
        authorization.style.display = "none";
        registration.style.display = "block";
        error.innerHTML = ' ';
    }
    //проверк формы авторизации
    var email_authorization = document.getElementById('email-authorization'),
        password_authorization = document.getElementById('password-authorization'),
        ok_authorization = document.getElementById('ok-authorization');
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    ok_authorization.onclick = function() {
        password_authorization.style.border = "1px solid  #c2c2c2";
        email_authorization.style.border = "1px solid  #c2c2c2";
        //проверка поля email
        if(!email_authorization.value || reg.test(email_authorization.value) == false) {
            email_authorization.style.border = "2px solid red";
            event.preventDefault();
        }
        //проверка поля пароля на пустоту
        if(!password_authorization.value) {
            password_authorization.style.border = "2px solid red";
            event.preventDefault();
        }
    }

    //проверк формы ркгистрации
    var name_registration = document.getElementById('name-registration'),
        surname_registration = document.getElementById('surname-registration'),
        email_registration = document.getElementById('email-registration'),
        password_registration = document.getElementById('password-registration'),
        confirm_password_registration = document.getElementById('confirm-password-registration'),
        verification_registration = document.getElementById('verification-registration'),
        ok_registration = document.getElementById('ok-registration'),
        verification_registration_border = document.getElementById('verification-registration-border');
        
    ok_registration.onclick = function() {
        name_registration.style.border = "1px solid  #c2c2c2";
        surname_registration.style.border = "1px solid  #c2c2c2";
        email_registration.style.border = "1px solid  #c2c2c2";
        password_registration.style.border = "1px solid  #c2c2c2";
        confirm_password_registration.style.border = "1px solid  #c2c2c2";
        verification_registration_border.style.border = "none";
        //проверка поля email
        if(!email_registration.value || reg.test(email_registration.value) == false) {
            email_registration.style.border = "2px solid red";
            event.preventDefault();
        }
        if(password_registration.value != confirm_password_registration.value) {
            password_registration.style.border = "2px solid red";
            confirm_password_registration.style.border = "2px solid red";
            event.preventDefault();
        }
        //проверка поля пароля на пустоту и на повторение
        if(!password_registration.value ) {
            password_registration.style.border = "2px solid red";
            event.preventDefault();
        }
        //проверка поля пароля на пустоту
        if(!confirm_password_registration.value ) {
            confirm_password_registration.style.border = "2px solid red";
            event.preventDefault();
        }
        //проверка поля имени на пустоту
        if(!name_registration.value ) {
            name_registration.style.border = "2px solid red";
            event.preventDefault();
        }
        //проверка поля фамилии на пустоту
        if(!surname_registration.value ) {
            surname_registration.style.border = "2px solid red";
            event.preventDefault();
        }
         //проверка поля фамилии на пустоту
        if(!verification_registration.checked ) {
            verification_registration_border.style.border = "2px solid red";
            event.preventDefault();
        }
    }

       //проверка формы создания статей
    var head_articles = document.getElementById('head-articles');
    var text_articles = document.getElementById('text-articles');
    var save_articles = document.getElementById('save-articles');
    var tags_articles = document.getElementById('tags-articles');
        
    save_articles.onclick = function() {
       head_articles.style.border = "1px solid  #c2c2c2";
       //text_articles.style.border = "1px solid  #c2c2c2";
       tags_articles.style.border = "1px solid  #c2c2c2";
        //проверка поля имени на пустоту
        if(!head_articles.value ) {
            head_articles.style.border = "2px solid red";
            event.preventDefault();
        } 
        //проверка поля имени на пустоту
        if(!tags_articles.value ) {
            tags_articles.style.border = "2px solid red";
            event.preventDefault();
        }



    }

}