 $('.slider').slick({
  infinite: true,
  initialSlide: 0,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
      {
        breakpoint: 1650,
        settings: {
          slidesToShow: 4,
        }
      },
       {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        }
      },
       {
        breakpoint: 950,
        settings: {
          slidesToShow: 2,
        }
      },
        
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });