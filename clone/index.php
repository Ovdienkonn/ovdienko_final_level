<?php
session_start();
include("view/header.php");
if (!isset($_SESSION['transition'])) {
$_SESSION['transition'] = 'blog';
}
switch (true) {
		case ($_SESSION['transition'] == 'blog'):
			include("view/blog.php");
			break;
		case ($_SESSION['transition'] == 'blog-details'):
			include("view/blog-details.php");
			break;
		case ($_SESSION['transition'] == 'create-post'):
			include("view/create-post.php");
			break;
		}
include("view/footer.php");
include("view/authorization.php");
include("view/registration.php");
 ?>
