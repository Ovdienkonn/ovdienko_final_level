<?php
session_start();
include("connection_bd.php");

if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = 1;
}
$notesOnPage = 9;
$from = ($page - 1)* $notesOnPage;

	$query = "SELECT * FROM article WHERE id > 0 LIMIT ?,?";
	$respon2 = $db->prepare("SELECT * FROM article WHERE id > 0 LIMIT ?,?");
	$respon2->bindValue(1, $from, PDO::PARAM_INT);
	$respon2->bindValue(2, $notesOnPage, PDO::PARAM_INT);
	$respon2->execute();
	
	while ($array = $respon2->fetch(PDO::FETCH_ASSOC)) {
		$day = date("d", strtotime($array['date_created']));
		$month = date("M", strtotime($array['date_created']));
		echo '<div class="blog-post__item">';
					echo '<div class="box-img">';
						echo '<img src="'.$array['img'].'" alt="images">';
						echo '<div class="data">';
						echo '<p>'.$day.'<span>'.$month.'</span></p>';
						echo '</div>';
					echo '</div>';
					echo '<div class="box-descriotion">';
						echo '<div class="box-descriotion__head">';
							echo '<h3>'.$array['head'].'</h3>';
						echo '</div>';
						echo '<div class="box-descriotion__text">';
						echo $array['text_articles'];
						echo '</div>';
						echo '<div class="box-descriotion__box-action">';
						echo '<a class="box-descriotion__box-action__btn" href="/php/transitions-details.php?id='.$array['id'].'">MORE</a>';
						echo '	<div class="box-action__comments">';
						echo '		<i class="fa fa-comment-o" aria-hidden="true"></i>';
						echo '		<a href="#">15 Comments</a>';
						echo '	</div>';
						echo '	<div class="box-action__like">';
						echo '		<i class="fa fa-heart-o" aria-hidden="true"></i>';
						echo '		<a href="#">51 Likes</a>';
						echo '	</div>		';				
						echo '</div>';
					echo '</div>';
				echo '</div>';




	}
	
	
?>