<?php
session_start();
include("connection_bd.php");

if (isset($_SESSION['id_post'])) {
	$id_post = $_SESSION['id_post'];
}
	//поиск статьи
	$query = "SELECT * FROM article WHERE id = ?";
	$respon = $db->prepare($query);
	$respon->bindValue(1, $id_post, PDO::PARAM_INT);
	$respon->execute();
while ($article = $respon->fetch(PDO::FETCH_ASSOC)) {
	//поиск пользователя создавшего статью
	$query2 = "SELECT * FROM user WHERE id = ?";
	$respon2 = $db->prepare($query2);
	$respon2->bindValue(1, $article['id_user'], PDO::PARAM_INT);
	$respon2->execute();
	$user = $respon2->fetch(PDO::FETCH_ASSOC);

	//перевод формата времени
	$day = date("d", strtotime($article['date_created']));
	$month = date("M", strtotime($article['date_created']));
			//вывод расшириный статьи
			echo '<div class="blog-details__box-img">
						<img src="'.$article['img'].'" alt="images">
						<div class="data">
							<p>'.$day.' <span>'.$month.'</span></p>
						</div>
				</div>
				<div class="blog-details__head">
					<h2>'.$article['head'].'</h2>	
				</div>
				<div class="blog-details__owner">
					<div class="owner__name">
						
						<a href="#"><i class="far fa-user"></i>'.$user['name'].'</a>
					</div>';
					
			if($_SESSION['id'] == $article['id_user']) {		
				echo	'<div class="owner__edit">
						<a href="#"><i class="fas fa-edit"></i>Edit</a>
					</div>
					<div class="owner__delete">
						<a href="#"><i class="far fa-trash-alt"></i> Delete</a>
					</div>';
			}
			echo '</div>
				<div class="blog-details__text">
					'.$article['text_articles'].'
				</div>
				<div class="blog-details__tags">
					<div class="share-social">
						<button><i class="fas fa-share-alt"></i></button>
						<a href="https://www.facebook.com/sharer/sharer.php?u=www.clone.ua" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="https://twitter.com/home?status=www.clone.ua Я скушаю все ваши печенюшки!!" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						<a href="https://pinterest.com/pin/create/button/?url=www.clone.ua&media=&description=\'Я скушаю все ваши печенюшки!!\'" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
					</div>
					<div class="tags">
						<a href="#"><i class="fas fa-tags"> Tag:</i> '.$article['tags'].'</a>
					</div>
				</div>';





}

?>