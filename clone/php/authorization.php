<?php
session_start();
session_unset();
include_once("connection_bd.php");

$email = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);
$_SESSION['input-email'] = $email;
$_SESSION['input-password'] = $password;

if (!empty($email) and filter_var($email, FILTER_VALIDATE_EMAIL) and !empty($password)) {
	$query = "SELECT * FROM user WHERE email = ? ";
	$respon = $db->prepare($query);
	$respon->execute([$email]);
	$user = $respon->fetch(PDO::FETCH_ASSOC);
	if (!empty($user)) {
		if ($user['password'] == $password) {
			session_unset();
			$_SESSION['id'] = $user['id'];
			$_SESSION['name'] = $user['name'];
			$_SESSION['surname'] = $user['surname'];
			$_SESSION['email'] = $user['email'];
		} else {
			$_SESSION['error-authorizatio'] = 'User password is entered incorrectly!';	
		}
	} else {
		$_SESSION['error-registration'] = 'This email does not exist!';
	}
}
	header('HTTP/1.1 200 OK');
	header('Location: http://ovdienkonn.beget.tech');
	exit;






?>

