<?php
session_start();
include_once("connection_bd.php");
$notesOnItemPage = 9;

	$query = "SELECT COUNT(*) as count FROM article ";
	$respon = $db->prepare($query);
	$respon->execute();
	$article = $respon->fetch(PDO::FETCH_ASSOC);
	
	$pagesCount = ceil($article['count'] / $notesOnItemPage);

	if (isset($_GET['page'])) {
	$page = $_GET['page'];
	}
	$center = ceil($pagesCount/2);

	$pagePlus = $page+1;
	$pageMinus = $page-1;

	if ($pageMinus >= 1) {
		echo "<div class=\"previous-pages\"><a href=\"?page=$pageMinus\"><i class='fas fa-chevron-left'></i></a></div>";
	}
  	echo '<div class="pages">';

	if ($page == 1) {
		echo "<a class=\"pagination-active\" href=\"?page=1\">1</a>";
	}
	
	if ( $page != 1 ){
    	echo "<a href=\"?page=1\">1</a>";
	} 
	if ( $page != $center and $center !=1 ) {
			echo "<a href=\"?page=$center\">$center</a>";
		}

	if ( $page != 1) {
		echo "<a class=\"pagination-active\" href=\"?page=$page\">$page</a>";
	} 
	if ( $pagesCount > $page) {
		echo "<a href=\"?page=$pagesCount\">$pagesCount</a>";
	}
	echo '</div>';
	if ($pagePlus <= $pagesCount) {
		echo "<div class=\"previous-pages\"><a href=\"?page=$pagePlus\"><i class='fas fa-chevron-right'></i></a></div>";
	}

?>