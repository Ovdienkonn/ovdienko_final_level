<?php
session_start();
session_unset();
include_once("connection_bd.php");

$name = htmlspecialchars($_POST['name']);
$surname = htmlspecialchars($_POST['surname']);
$birth = htmlspecialchars($_POST['birth']);
$gender = htmlspecialchars($_POST['gender']);
$email = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);
$confirm_password = htmlspecialchars($_POST['confirm-password']);
$verification = htmlspecialchars($_POST['verification']);
	//заносим перемнные в ссесию для исправления ошибок если они возникнут
	$_SESSION['name-registration'] = $name ;
	$_SESSION['surname'] = $surname;
	$_SESSION['birth'] = $birth;
	$_SESSION['gender'] = $gender;
	$_SESSION['email'] = $email;
	$_SESSION['password'] = $password;
	$_SESSION['confirm_password'] = $confirm_password;
	$_SESSION['verification'] = $verification;

if ($verification == 1) {

	switch (true) {
		case (empty($name)):
			$_SESSION['error-registration'] = 'User name is entered incorrectly!';
			break;
		case (empty($surname)):
			$_SESSION['error-registration'] = 'User surname is entered incorrectly!';
			break;
		case (empty($birth)):
			$_SESSION['error-registration'] = 'User birth is entered incorrectly!';
			break;
		case (empty($gender)):
			$_SESSION['error-registration'] = 'User gender is entered incorrectly!';
			break;
		case (empty($email) and !filter_var($email, FILTER_VALIDATE_EMAIL) ):
			$_SESSION['error-registration'] = 'User email is entered incorrectly!';
			break;
		case (empty($password)):
			$_SESSION['error-registration'] = 'User password is entered incorrectly!';
			break;
		case (empty($confirm_password)):
			$_SESSION['error-registration'] = 'User confirm_password is entered incorrectly!';
			break;
		case ($confirm_password != $password ):
			$_SESSION['error-registration'] = 'User confirm-password and password is entered incorrectly!';
			break;
	}
	$query = "INSERT INTO user (id, name, surname, birth, gender, email, password) VALUES ( ?, ?, ?, ?, ?, ?, ?)";
	$respon = $db->prepare($query);
	$respon->execute([NULL, $name, $surname, $birth, $gender, $email, $password]);
	$user_id = $db->lastInsertId();
	session_unset();
	$_SESSION['name'] = $name;
	$_SESSION['surname'] = $surname;
	$_SESSION['email'] = $email;
	$_SESSION['id'] = $user_id;
}
	header('HTTP/1.1 200 OK');
	header('Location: http://ovdienkonn.beget.tech');
	exit;






?>

