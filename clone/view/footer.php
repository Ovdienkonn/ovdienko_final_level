
	<section class="brends">
		<div class="brends__title">
			<h1>OUR BRANDS</h1>
			<img src="img/slayer-2.png" alt="img">
		</div>
			<div class="slider">
				<div class="slider_item">
					<img src="img/1.png" alt="">
				</div>
				<div class="slider_item">
					<img src="img/2.png" alt="">
				</div>
				<div class="slider_item">
					<img src="img/3.png" alt="">
				</div>
				<div class="slider_item">
					<img src="img/4.png" alt="">
				</div>
				<div class="slider_item">
					<img src="img/5.png" alt="">
				</div>
				<div class="slider_item"> 
					<img src="img/6.png" alt="">
				</div>
				<div class="slider_item"> 
					<img src="img/6.png" alt="">
				</div>
			</div>
	</section>
	<section class="newsletter">
		<div class="newsletter-up">
			<div class="newsletter__left">
				<img src="img/left.png" alt="Стразики">
			</div>
			<div class="newsletter__center">
				<div class="newsletter__center-box">
					<div class="newsletter__center-box__position">
						<div class="center-box__title">
							<h3>NEWSLETTER</h3>
							<img src="img/slayer-3.png" alt="img">
						</div>
						<form>
							<input class="center-box__inpt-subscribe" type="text" name="subscribe" placeholder="Enter your Email">
							<button class="center-box__btn-subscribe">SUBSCRIBE</button>
						</form>
					</div>
				</div>
			</div>
			<div class="newsletter__right">
				<img src="img/right.png" alt="Стразики">
			</div>
		</div>	
	</section>
	<footer>
		<div class="footer__top">
			<div class="border-info">
				<div class="footer__top__menu">
					<div class="menu__my-account">
						<div class="footer__menu__head">MY ACCOUNT</div>
						<div class="line size-center color-dark"></div>
						<ul class="footer__menu__list" >
							<li><a href="#">My Orders</a></li>
							<li><a href="#">My Credit Slips</a></li>
							<li><a href="#">My Addresses</a></li>
							<li><a href="#">My Personal Info</a></li>
						</ul>
					</div>
					<div class="menu__orders">
						<div class="footer__menu__head">ORDERS</div>
						<div class="line size-center color-dark"></div>
						<ul class="footer__menu__list" >
							<li><a href="#">Payment Option</a></li>
							<li><a href="#">Shipping</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="#">Delivery</a></li>
						</ul>
					</div>
					<div class="menu__information">
						<div class="footer__menu__head">NFORMATION</div>
						<div class="line size-center color-dark"></div>
						<ul class="footer__menu__list" >
							<li><a href="#">About us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Custom Service</a></li>
						</ul>
					</div>
					<div class="menu__contact">
						<div class="footer__menu__head">CONTACT US</div>
						<div class="line size-center color-dark"></div>
						<ul class="footer__menu__list" >
							<li><a class="adress" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>68 Dohava Stress, Lorem isput Spusts <br>New York- United State</a></li>
							<li><a class="mail" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> info@youremail.com</a></li>
							<li><a class="phone" href="#"><i class="fa fa-phone" aria-hidden="true"></i>+1 (00) 42 868 666 888</a></li>
						</ul>
					</div>
				</div>
				<div class="line size-max color-dark"></div>
				<div class="footer__bottom">
					<div class="bottom__social">
						<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
					</div>
					
					<div class="footer__bootom__card">
						<img src="img/card-5.png" alt="card">
						<img src="img/card-4.png" alt="card">
						<img src="img/card-3.png" alt="card">
						<img src="img/card-2.png" alt="card">
						<img src="img/card-1.png" alt="card">
					</div>
				</div>
			</div>	

		</div>
		
		<div class="copyright">
			<div class="btn-up">
			<a href="#head"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
			</div>
			<div class="copyright_text">
				<p>Copyright © 2016, <span>Theme_Express </span>All Rights Reseved.</p>
			</div>
			
		</div>
	</footer>


<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/jquery-migrate-1.4.1.min.js"></script>
<script src="slick/slick.min.js"></script>
<script src="js/slider.js"></script>
<script src="js/function.js"></script>

</body>
</html>