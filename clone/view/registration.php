<?php
	if (isset($_SESSION['error-registration'])) {
		echo '<style> #registration { display: block; } </style>';
	}
?>
<div id="registration">
		<div class="window size-registration">
			<button class="close"> &times;</button>
			<div class="head-form">Registration</div>
			 <form action="../php/registration.php" method="post">
				<label> <span id="error"><?php echo $_SESSION['error-registration']; unset($_SESSION['error-registration']); ?></span></label>
			 	 <input type="text" name="name" value = "<?php if (isset($_SESSION['name-registration'])) {echo $_SESSION['name-registration'];} ?>" placeholder="Name" id="name-registration">
			 	 <input type="text" name="surname" value = "<?php if (isset($_SESSION['surname'])) {echo $_SESSION['surname'];} ?>" placeholder="Surname" id="surname-registration">
			 	 <input type="date" name="birth">
				 <div class="gender">
	    			 <label><input type="radio" name="gender" value="Woman" <?php if (isset($_SESSION['gender']) and $_SESSION['gender'] == 'Woman') { echo 'checked';} ?> > Woman</label>
	    			 <label><input type="radio" name="gender" value="Man" <?php if (isset($_SESSION['gender']) and $_SESSION['gender'] == 'Man') { echo 'checked';} elseif ($_SESSION['gender'] != 'Woman') { echo 'checked';}?> > Man</label>
    			 </div>
    			 <input type="text" name="email" value = "<?php if (isset($_SESSION['emaile'])) {echo $_SESSION['email'];} ?>" placeholder="Email" id="email-registration">
				 <input type="password" name="password" value = "<?php if (isset($_SESSION['password'])) {echo $_SESSION['password'];} ?>" placeholder="Password" id="password-registration">
				 <input type="password" name="confirm-password" value = "<?php if (isset($_SESSION['confirm_password'])) {echo $_SESSION['confirm_password'];} ?>" placeholder="Confirm-password" id="confirm-password-registration">
				 <label> <span id="verification-registration-border"></span><input type="checkbox" name="verification" value="1" id="verification-registration">I agree to the terms ..</label>
				 <input type="submit" value="OK" id="ok-registration">
			</form>
		</div>
</div>