	<section class="blog-details">
		<div class="border-info top">
			<div class="blog-details__left-box">
				
	<?php	include('php/blog-details.php'); ?>

			</div>
			<aside class="blog-details__side-bar">
				<div class="side-bar__search">
					<form>
						<input class="input-search-min" type="text" name="search" placeholder="Search...">
						<button class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
					</form>
				</div>
				<div class="blog-details__categories">
					<h4>CATEGORT</h4>
					<div class="line size-min color-dark"></div>
					<ul>
						<li><a href="#">Rings (268)</a></li>
						<li><a href="#">Necklaces (96)</a></li>
						<li><a href="#">Earrings (873)</a></li>
						<li><a href="#">Bracelets (622)</a></li>
						<li><a href="#">Bangles (187)</a></li>
						<li><a href="#">Beads & Charms (93)</a></li>
						<li><a href="#">Jewellery Boxes (52) </a></li>
					</ul>
					<div class="line size-max color-dark"></div>
				</div>
				<div class="blog-details__recent-post">
					<h4>RECENT POST</h4>
					<div class="recent-post__box">
						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-1.png" alt="box">
							</div>
							<div class="item_text">
								<h5>Your Blog Title goes Here</h5>
								<span>15 May, 2015</span>
							</div>
						</div>
						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-1.png" alt="box">
							</div>
							<div class="item_text">
								<h5>Something is Going Awesome</h5>
								<span>09 Jan, 2016</span>
							</div>
						</div>
						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-1.png" alt="box">
							</div>
							<div class="item_text">
								<h5>Your Blog Title goes Here</h5>
								<span>15 May, 2015</span>
							</div>
						</div>
						
					</div>
				</div>
				<div class="line size-max color-dark"></div>
				<div class="blog-details__on-sale">
					<h4>ON SELL</h4>
					<div class="recent-post__box">

						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-2.png" alt="box">
							</div>
							<div class="item_text on-sale__text">
								<h5>Diamond Five Wow Ring</h5>
								<span>$520</span>
							</div>
						</div>

						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-2.png" alt="box">
							</div>
							<div class="item_text on-sale__text">
								<h5>Men,s Fashion Watch</h5>
								<span>$210</span>
							</div>
						</div>

						<div class="recent-post__item">
							<div class="item_img">
								<img src="img/Shape-2.png" alt="box">
							</div>
							<div class="item_text on-sale__text">
								<h5>Diamond Engagement Ring</h5>
								<span>$352</span>
							</div>
						</div>
						
					</div>
				</div>
				<div class="line size-max color-dark"></div>

				<div class="blog-details__shop-now">
					<a href="#"><img src="img/SHOP-NOW.png" alt="SHOP-NOW"></a>
				</div>
			</aside>
		</div>
	</section>