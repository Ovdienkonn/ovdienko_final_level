<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CLONE</title>
	<link rel="stylesheet" type="text/css" href="../css/font.css">
	<link rel="stylesheet" href="../font/font-awesome/css/all.css">
	<link rel="stylesheet" href="../font/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="../slick/slick-theme.css"/>
	<script src="../js/jquery-3.0.0.min.js"></script>
	<script src="../ckeditor/ckeditor.js"></script>
</head>
<body>

	<header id="head">
		<div class="top-bar">
			<div class="border-info">
				<div class="top-bar__mail">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					info@youremail.com	
				<div class="vertical-line"></div>	
				</div>
				
				<div class="top-bar__call">
					<i class="fa fa-phone" aria-hidden="true"></i>
					+(56) 123 456 546
				</div>
				
				<div class="top-bar__account">
					<div class="vertical-line"></div>	
					<a class="account"<?php if(isset($_SESSION['name'])) { echo 'id="user-account"';} else { echo 'id="my-account"';} ?> > My Account</a>
					<div id="user-menu">
						<a > <?php echo $_SESSION['name'].' '.$_SESSION['surname']; ?></a>
						<a href="../php/transition-article.php">Article creation</a>
						<a href="../php/logout.php">LogOut</a>
					</div>
				</div>
				<div class="top-bar__name">
					<span><?php echo $_SESSION['name']; ?></span>
				</div>
				
			</div>
			
		</div>
		
		<div class="border-info">
			<div class="top-center">	
				<div class="top-center__logo">
					<img src="img/Logo.png" alt="log">
				</div>
				<div class="top-center__search">
					<form>
						<input class="input-search" type="text" name="search" placeholder="Inter Your Keyword. . .">
						<button class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
					</form>
				</div>
			</div>
		</div>
		<div class="line size-max color-light"></div>

		<div class="border-info">
			<div class="menu-bar">
<?php if($_SESSION['transition'] != 'blog' and isset($_SESSION['transition']) != false) { ?>			
				<div class="menu-bar__top-menu">
					<ul>
						<li><a href="../php/return-home.php">Home</a></li>
						<li><a href="#">Man</a></li>
						<li><a href="#">Women</a></li>
						<li><a href="#">Kids</a></li>
						<li><a href="#">Accoseriese</a></li>
						<li><a href="#">Featured</a></li>
					</ul>		
				</div>
				<div class="menu-bar__top-social">
						<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
				</div>
<?php  }?>				
			</div>
		</div>

	</header>
	<section class="banner">
	<div class="banner__title">
			<h2>Blog</h2>
			<h1>Latest News</h1>
			<h4>Home  <i class="fa fa-angle-right" aria-hidden="true"></i>  <span><?php if($_SESSION['transition'] == 'blog') {echo 'News'; }else {echo 'News Details';}?>	</span></h4>
		</div>
	</section>